# Chef library

Chef Library - C++ Highly Effective Functional Library

This is a C++ library that enables some function programming techniques in C++
and provides several platform-independant building blocks for firmware and
embedded software.

The library is based on my previous experience. Most of the code is a 
rewrite of components I developed in my professional life. I am sure that 
all the code is enough different to avoid any copyright infringement claim. 


## Description

Chef goal is to provide a platform-independent swiss-army-knife library for 
C++ programming with a specific attention to correctness and functional
programming approach.

There are two guiding principles -
* it shall be impossible to misuse the components;
* no exceptions

Currently, the library is written in C++17, but I foresee a port to C++20 in 
the future.

Idiomantic C++ is used whenever possible.


## Usage

Using Chef should be straightforward as long as you use a gcc compiler. If 
you want to implement support for other compiler you are encouraged to send 
a Merge Request.

Although the library can be compiled both as a static or shared library, the
supported inclusion mode is via git submodule, so that Chef directory tree is
actually included in the directory tree of your project.


## Documentation

All the library code is documented using doxygen comments. Higher level 
documentation, describing the architecture and the design choices, is 
available in the Docs directory.


## Support

The code is provided with no support whatsoever, paid or free. The intended 
audience is composed by programmers that should be able to find their way 
using the code.

Bug reports are welcome, if well documented, but I'm not bound by any mean 
to look into it and/or fix it. Sorry, but I prefer to be clear about what I can
and can't do for you.


## Contribution

You are encouraged to contribute via merge requests. Please follow the code 
layout and style you find in the source code.

If adding new functions or components, the corresponding unit test should be 
provided as well.


## License

The Chef library is provided under the Apache License v2.0. See file LICENSE 
in the project main directory.

## Authors and Acknoledgements

I'd like to thank
[MR&D Institute](https://web.archive.org/web/20140517204627/http://mrd-institute.com/),
[Tecniplast SpA](https://www.tecniplast.it/) and
[SISSPre srl](https://www.sisspre.it/) for the free and welcoming environment
that they offered, so that I could experiment and get useful insight on the
creation and mantaining of C and C++ firmware and embedded libraries.

In these companies many people provided valuable insights and feedbacks to my
programming approach. In a somewhat chronological order I'd like to thank
Timoteo Pascale, Luca Borsani, Andrea Giacosi, Marco Garzola, Carmelo Pintaudi,
Sandro Mosca, Francesco Cusumano and, Roberto Spelta.


## Projecct Status

Chef is currently in alpha status, although the interface should be quite
stable. There is no defined readmap, but soon or later a first release will be
issued.
