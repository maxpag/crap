/**
 * @file x86_generic/ChefMachine/Machine_Impl.hh
 * @date 2022-06-03
 * @author Massimiliano Pagani
 * @addtogroup ChefMachine
 * @{
 */

#if !defined(CHEF_MACHINE_IMPL_HH)
#define CHEF_MACHINE_IMPL_HH

#if !defined( CHEF_MACHINE_HH )
#  error "This file is not meant to be included directly, include <ChefMachine/Machine.hh>"
#endif

namespace ChefMachine
{

    inline uintptr_t getSp() noexcept
    {
        register uintptr_t sp asm("sp");
        return sp;
    }

    inline uintptr_t getPc() noexcept
    {
        auto getPcImpl = []() __attribute__((__noinline__))
        {
            return reinterpret_cast<uintptr_t>(__builtin_return_address(0));
        };
        return getPcImpl();
    }

    #define ChefMachine_DEBUG_HALT() asm("int $3")
}

#endif//CHEF_MACHINE_IMPL_HH
