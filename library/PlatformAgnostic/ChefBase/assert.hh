/**
 * @addtogroup ChefBase
 * @{
 * @file ChefBase/assert.hh
 * @author Massimiliano Pagani
 * @date 2014-10-17
 *
 */

#if !defined( CHEFBASE_ASSERT_HH )
#define CHEFBASE_ASSERT_HH

#include <ChefSystem/System.hh>
#include <ChefMachine/Machine.hh>
#include <experimental/source_location>

namespace ChefBase
{
    namespace Assert
    {
#if defined(ChefBase_BRIEF_ASSERT)
        typedef void CallBackFn(uintptr_t pc, uintptr_t sp) noexcept;
#elif defined(ChefBase_FULL_ASSERT)
        typedef void CallBackFn(
            std::experimental::source_location const& location,
            char const* text
        ) noexcept;
#else
#error "assert mode not defined (either ChefBase_FULL_ASSERT or ChefBase_BRIEF_ASSERT)"
#endif

        extern CallBackFn* g_assertCallback;

#if defined(ChefBase_BRIEF_ASSERT)
        inline void callback() noexcept
        {
            g_assertCallback( ChefMachine::getPc(), ChefMachine::getSp());
        }

#elif defined(ChefBase_FULL_ASSERT)

        inline void
        callback(
            std::experimental::source_location const& location,
            char const* text
        ) noexcept
        {
            g_assertCallback( location, text );
        }
#endif

        inline void
        failAction(
            char const* failedCondition,
            std::experimental::source_location const& location = std::experimental::source_location::current()
        ) noexcept
        {
#if defined(BUILD_RELEASE)
#  if defined( ChefBase_FULL_ASSERT )
            callback( location, failedCondition );
#  elif defined( ChefBase_BRIEF_ASSERT )
            (void)failedCondition;
            (void)location;
            callback();
#  endif
#else
            (void)failedCondition;
            (void)location;
            ChefMachine_DEBUG_HALT();
#endif
            ChefSystem::abort();
        }

        void setCallback(CallBackFn * callback) noexcept;

    }

}

#define ChefBase_ASSERT(X)                              \
        do                                              \
        {                                               \
            if(!(X))                                    \
            {                                           \
                ChefBase::Assert::failAction(#X);       \
                __builtin_unreachable();                \
            }                                           \
        } while(false)

#define ChefBase_ASSERT_FAIL()  ChefBase_ASSERT(false);


#endif  /* CHEFBASE_ASSERT_HH */
///@}
