/**
 * @file ChefBase/Error.hh
 * @date 2020-03-19
 * @author Massimiliano Pagani
 * @addtogroup ChefBase
 * @{
 */

/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#if !defined( CHEFBASE_ERROR_HH )
#define CHEFBASE_ERROR_HH

#include <cstdint>
#include <array>
#include <ChefBase/assert.hh>
#include <ChefFun/Either.hh>

namespace ChefBase
{
/**
 * Error represents a generic Error. Hopefully this is going to replace all
 * the needs for exception when used together with Either to return
 * function results.
 *
 * Usage.
 *
 * In your compilation unit write the following header, customized to suit
 * your needs:
 * @code
 *  #include <ChefBase/Error.hh>
 *
 *  ChefBase::LogModule g_error{ "ModuleName" };    // replace with human
 *                                                  // readable module name.
 *
 *  enum ErrorCodes         // These are the codes of the specific errors that
 *  {                       // may be encountered in your module. Each module
 *      OK,                 // has its own kind of errors.
 *      TIMEOUT,            // First error code, must always be OK to indicate
 *      INVALID_DATE,       // no error.
 *      INVALID_TIME
 *  };
 *
 *  // you may want to define the following commodity function to create Error
 *  // instances on the fly.
 *
 *  inline auto MakeError( ErroCodes code, unsigned data =  0 ) -> ChefBase::Error
 *  {
 *      return ChefBase::Error( code, g_error.getCode(), data );
 *  }
 *
 * @endcode
 *
 */

    class Error
    {
        public:
            /**
             * Builds an error instance.
             *
             * @param errorCode the error code. If this is zero, then it means
             *                  conventionally no error.
             * @param errorModule the error module where the error occurred. You
             *                    get this code by calling getCode() on
             *                    LogModule instance.
             * @param data optional data to add information about the error.
             */
            constexpr Error(
                uint8_t errorCode,
                uint8_t errorModule,
                uint16_t data = 0
            ) noexcept;

            [[nodiscard]] bool isError() const noexcept;

            [[nodiscard]] bool isOk() const noexcept;

            [[nodiscard]] unsigned getCode() const noexcept;

            [[nodiscard]] unsigned getModule() const noexcept;

            [[nodiscard]] unsigned getData() const noexcept;

            template<typename F>
            Error andThen( F f ) const noexcept( std::is_nothrow_invocable_v<F> );

            template<typename F>
            Error orElse( F f ) const noexcept( std::is_nothrow_invocable_v<F> );

            template<typename T>
            ChefFun::Either<Error,T> toEither( T data ) const noexcept;

            static Error const Ok;
        private:
            friend bool
            operator==( Error lhs, Error rhs ) noexcept;

            uint8_t m_code;
            uint8_t m_module;
            uint16_t m_data;
    };

    // ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
    // ::::::::::::::::::::::::::::::::::::::::::::::::::::: inline methods :::
    // ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

    inline bool Error::isError() const noexcept
    {
        return m_code != 0;
    }

    inline bool Error::isOk() const noexcept
    {
        return m_code == 0;
    }

    inline unsigned Error::getCode() const noexcept
    {
        return m_code;
    }

    inline unsigned Error::getModule() const noexcept
    {
        return m_module;
    }

    inline unsigned Error::getData() const noexcept
    {
        return m_data;
    }

    inline constexpr
    Error::Error( uint8_t errorCode, uint8_t errorModule, uint16_t data ) noexcept
        : m_code( errorCode )
        , m_module( errorModule )
        , m_data( data )
    {
    }

#if 0
    template <typename T>
    inline Error LogModule::make( T errorCode, unsigned int data ) const noexcept
    {
        return Error( static_cast<uint8_t>(errorCode), data );
    }
#endif

    template<typename F>
    inline Error
    Error::andThen( F f ) const noexcept( std::is_nothrow_invocable_v<F> )
    {
        return m_code == 0 ?
            f() :
            *this;
    }

    template<typename F>
    inline Error
    Error::orElse( F f ) const noexcept( std::is_nothrow_invocable_v<F> )
    {
        return m_code == 0 ?
            *this :
            f( *this );
    }

    template<typename T>
    ChefFun::Either<Error,T> Error::toEither( T data ) const noexcept
    {
        return m_code == 0 ?
            ChefFun::Either<Error,T>::Right( std::forward<T>( data ) ) :
            ChefFun::Either<Error,T>::Left( *this );
    }

    inline bool
    operator==( Error lhs, Error rhs ) noexcept
    {
        return lhs.m_code == rhs.m_code &&
               lhs.m_module == rhs.m_module &&
               lhs.m_data == rhs.m_data;
    }
}

#endif //CHEFBASE_ERROR_HH

///@}
