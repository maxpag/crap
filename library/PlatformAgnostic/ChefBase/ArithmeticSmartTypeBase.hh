/**
 * @file ArithmeticSmartTypeBase.hh
 * @date 2022-01-20
 * @author Massimiliano Pagani
 * @addtogroup ChefBase
 * @{
 */

#if !defined( CHEFBASE_ARITHMETICSMARTTYPEBASE_HH )
#define CHEFBASE_ARITHMETICSMARTTYPEBASE_HH

#include <type_traits>

namespace ChefBase
{
    /**
     * SmartType base for arithmetic types. Suppose you want to define your own
     * float to represent a Speed, so that the compiler is able, via
     * typechecking, to ensure that  your code is correct and you are not
     * supplying a Speed where an Acceleration is needed.
     *
     * Note that this has nothing to do with dimensional enforcement. Types can
     * help you to ensure that m/s are passed where m/s are required, but you
     * may need more control over which speed is which. You may want a WindSpeed
     * that is treated differently from AircraftSpeed.
     *
     * The template ArithmeticSmartTypeBase provides a wrapper around a numeric
     * type of your choice and via CRTP, is a base for your specific smart type.
     *
     * Heirs must be defined as follows:
     *
     * @code
     * class Speed : public ArithmeticSmartTypeBase<Speed,float>
     * {
     * ...
     *     private:
     *         explicit Speed( float n )
     *             : ArithmeticSmartTypeBase<Speed,float>{ n }
     *         {}
     *         friend ArithmeticSmartTypeBase<Speed,float>;
     * };
     * @endcode
     *
     * @note no virtual function is defined, therefore you are not supposed to
     *       pass around references to the base class.
     *
     * The defined class must declare an (explicit) constructor that takes a
     * single argument of the underlying type and forwards it to the
     * ArithmeticSmartTypeBase<T,N> constructor.
     *
     * Also the derived class must be friend to
     * ArithmeticSmartTypeBase<Speed,float> in order to access the constructor.
     *
     * Likely the derived class will have other constructors, or (better)
     * factories so to force the build of smart types from known values.
     *
     * In order to access the underlying value, the derived class may use the
     * protected method getValue().
     *
     * All methods in ArithmeticSmartTypeBase<T,N> are constexpr and none throws
     * exception.
     */

    template <typename T, typename N>
    class ArithmeticSmartTypeBase
    {
        public:
            /**
             * Relational operators. Operators are forwarded to the same
             * operators of the underlying type.
             *
             * @param other the other object to compare
             * @return the boolean result of the comparison.
             * @{
             */
            [[nodiscard]] bool constexpr operator==( T other ) const noexcept;
            [[nodiscard]] bool constexpr operator<( T other ) const noexcept;
            /** @} */

            /**
             * Math operators. Operators are forwarded to the same operators of
             * the underlying type.
             *
             * @note no check is performed to avoid division by zero. This is
             *       up to the calling code (if needed).
             *
             * @param other the other object
             * @return the result wrapped in a ArithmeticSmartTypeBase
             * @{
             */
            [[nodiscard]] constexpr T operator+( T other ) const noexcept;
            [[nodiscard]] constexpr T operator-( T other ) const noexcept;

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wnon-template-friend"
            friend constexpr T operator*( T lhs, T rhs ) noexcept;
            template<typename U>
            friend constexpr T operator*( U lhs, T rhs ) noexcept;
            template<typename U>
            friend constexpr T operator*( T lhs, U rhs ) noexcept;
#pragma GCC diagnostic pop
            [[nodiscard]] constexpr N operator/( T other ) const noexcept;
            template<typename U>
            [[nodiscard]] constexpr T operator/( U other ) const noexcept;
            /** @} */

        protected:
            /**
             * Constructor. This is intended for heir to construct an object
             * with the given value. It is up to the derived class to construct
             * only "valid" objects.
             *
             * @param n the value of the object.
             */
            explicit constexpr ArithmeticSmartTypeBase( N n ) noexcept;

            /**
             * Getter of the value. By using this function (and this function
             * alone, the heir may access the value of the object. The value is
             * never mutated, so it is exactly the value provided in the
             * constructor of the same object.
             *
             * @return the value of the object.
             */
            [[nodiscard]] constexpr N getValue() const noexcept;
        private:
            [[nodiscard]] constexpr T multiply( T rhs ) const noexcept;
            template<typename U>
            [[nodiscard]] constexpr T multiplyOther( U u ) const noexcept;
            template<typename U>
            [[nodiscard]] constexpr N divide( U u ) const noexcept;
            template<typename U>
            [[nodiscard]] constexpr T divideOther( U u ) const noexcept;

            [[nodiscard]] friend constexpr T operator*( T lhs, T rhs ) noexcept
            {
                return lhs.multiply( rhs );
            }

            template<typename U>
            [[nodiscard]] friend constexpr T operator*( U lhs, T rhs ) noexcept
            {
                return rhs.multiplyOther( lhs );
            }

            template<typename U>
            [[nodiscard]] friend constexpr T operator*( T lhs, U rhs ) noexcept
            {
                return lhs.multiplyOther( rhs );
            }

            N m_n;
    };

    /**
     * Derived relational operators. These are generic operators that derives
     * the corresponding comparison by using '<' and '=='.
     *
     * @tparam T The type of the class which provides '<' and '=='
     * @param lhs the left hand side operand
     * @param rhs the right hand side operand
     * @return the boolean result of the comparison.
     * @{
     */
    template <typename T>
    [[nodiscard]] bool constexpr operator!=( T lhs, T rhs ) noexcept;
    template <typename T>
    [[nodiscard]] bool constexpr operator>( T lhs, T rhs ) noexcept;
    template <typename T>
    [[nodiscard]] bool constexpr operator>=( T lhs, T rhs ) noexcept;
    template <typename T>
    [[nodiscard]] bool constexpr operator<=( T lhs, T rhs ) noexcept;
    /** @} */

    // ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
    // :::::::::::::::::::::::::::::::::::::::::::::::::::: implementations :::
    // ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

    template<typename T>
    [[nodiscard]] inline constexpr T operator*( T lhs, T rhs ) noexcept
    {
        return lhs.multiply( rhs );
    }

    template <typename T, typename N>
    [[nodiscard]] inline constexpr bool
    ArithmeticSmartTypeBase<T,N>::operator==( T other ) const noexcept
    {
        return m_n == other.m_n;
    }

    template <typename T, typename N>
    [[nodiscard]] inline constexpr bool
    ArithmeticSmartTypeBase<T,N>::operator<( T other ) const noexcept
    {
        return m_n < other.m_n;
    }

    template <typename T, typename N>
    [[nodiscard]] inline constexpr T
    ArithmeticSmartTypeBase<T,N>::operator+( T other ) const noexcept
    {
        return T{ m_n+other.m_n };
    }

    template <typename T, typename N>
    [[nodiscard]] inline constexpr T
    ArithmeticSmartTypeBase<T,N>::operator-( T other ) const noexcept
    {
        return T{ m_n-other.m_n };
    }

    template <typename T, typename N>
    [[nodiscard]] inline constexpr N
    ArithmeticSmartTypeBase<T,N>::operator/( T other ) const noexcept
    {
        return m_n/other.m_n;
    }

    template <typename T, typename N>
    inline constexpr T
    ArithmeticSmartTypeBase<T,N>::multiply( T rhs ) const noexcept
    {
        return T{ m_n*rhs.m_n };
    }

    template <typename T, typename N>
    template<typename U>
    inline constexpr T
    ArithmeticSmartTypeBase<T,N>::multiplyOther( U u ) const noexcept
    {
        return T{ m_n*u };
    }

    template <typename T, typename N>
    template<typename U>
    inline constexpr T
    ArithmeticSmartTypeBase<T,N>::divideOther( U u ) const noexcept
    {
        return T{ m_n/u };
    }

    template <typename T, typename N>
    template<typename U>
    [[nodiscard]] inline constexpr T
    ArithmeticSmartTypeBase<T,N>::operator/( U other ) const noexcept
    {
        return T{ m_n/other };
    }

    template <typename T, typename N>
    inline constexpr
    ArithmeticSmartTypeBase<T,N>::ArithmeticSmartTypeBase( N n ) noexcept
        : m_n{n}
    {
    }

    template <typename T, typename N>
    [[nodiscard]] constexpr N
    ArithmeticSmartTypeBase<T,N>::getValue() const noexcept
    {
        return m_n;
    }

    template <typename T>
    [[nodiscard]] inline constexpr bool operator!=( T lhs, T rhs ) noexcept
    {
        return !(lhs == rhs);   // NOLINT
    }

    template <typename T>
    [[nodiscard]] inline constexpr bool operator>( T lhs, T rhs ) noexcept
    {
        return !(lhs < rhs) && !(lhs == rhs); // NOLINT
    }

    template <typename T>
    [[nodiscard]] inline constexpr bool operator>=( T lhs, T rhs ) noexcept
    {
        return !(lhs < rhs);    // NOLINT
    }

    template <typename T>
    [[nodiscard]] inline bool constexpr operator<=( T lhs, T rhs ) noexcept
    {
        return (lhs < rhs) || (lhs == rhs);
    }
}

#endif //CHEFBASE_ARITHMETICSMARTTYPEBASE_HH

/// @}
