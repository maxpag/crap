/**
 * @file System.hh
 * @date 2022-06-03
 * @author Massimiliano Pagani
 * @addtogroup ChefSystem
 * @{
 */

/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#if !defined(CHEF_SYSTEM_HH)
#define CHEF_SYSTEM_HH

namespace ChefSystem
{
    /**
     * Terminates the execution of the program and hands back the control to
     * the hosting system.
     *
     * If the hosting system is a realtime scheduler or the code is running on
     * the bare metal, then this will cause a system reset.
     *
     * @note the function does not return.
     */
    void abort() noexcept __attribute__ ((__noreturn__));;
}

#endif//CHEF_SYSTEM_HH

/** @} */