/**
 * @file Either_L_void.hh
 * @author Massimiliano Pagani
 * @addtogroup ChefFun
 * @{
 */

#if !defined( CHEFFUN_EITHER_HH )
#error "This file is not intended to be included directly, use <ChefFun/Either.hh>"
#endif //CHEFFUN_EITHER_HH


template<typename L>
class Either<L,void>
{
    public:
        static Either<L,void> Right() noexcept;
        static Either<L,void> Left( L&& left ) noexcept;
        static Either<L,void> Left( L left ) noexcept;

        template<typename F>
        Either<L,std::invoke_result_t<F>> map( F f ) const noexcept( noexcept(f() ));


        template<typename F>
        Either<L,std::invoke_result_t<F>> matchRight( F f ) const noexcept( noexcept(f() ));

        template<typename F>
        void matchLeft( F f ) const noexcept( noexcept( f( std::declval<L>() )));

        [[nodiscard]] bool isRight() const noexcept;
        [[nodiscard]] bool isLeft() const noexcept;

        [[nodiscard]] L const& getLeft() const noexcept;
    private:
        explicit Either( L&& left );
        explicit Either( L left );
        Either();

        Option<L> m_left;
};

// ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// ::::::::::::::::::::::::::::::::::::::::::::::::::::::::: implementation :::
// ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

template<typename L>
Either<L,void>::Either( L&& left )
    : m_left{ left }
{}

template<typename L>
Either<L,void>::Either( L left )
    : m_left{ Option<L>::Some(left) }
{}

template<typename L>
Either<L,void>::Either()
    : m_left{ Option<L>::None() }
{
}

template<typename L> inline
bool Either<L,void>::isRight() const noexcept
{
    return m_left.isEmpty();
}

template<typename L> inline
bool Either<L,void>::isLeft() const noexcept
{
    return m_left.isDefined();
}

template<typename L>
Either<L,void> Either<L,void>::Right() noexcept
{
    return Either<L,void>{};
}

template<typename L>
Either<L,void> Either<L,void>::Left( L&& left ) noexcept
{
    return Either<L,void>{ left };
}

template<typename L>
Either<L,void> Either<L,void>::Left( L left ) noexcept
{
    return Either<L,void>{ left };
}


template<typename L> inline
L const& Either<L,void>::getLeft() const noexcept
{
    return m_left.get();
}

template<typename L>
template<typename F>
Either<L,std::invoke_result_t<F>> Either<L,void>::map( F f ) const noexcept( noexcept(f() ))
{
    using TargetType = std::invoke_result_t<F>;

    return isRight() ?
        Either<L,TargetType>::Right( f() ) :
        Either<L,TargetType>::Left( getLeft() );
}

template<typename L>
template<typename F>
inline
Either<L,std::invoke_result_t<F>> Either<L,void>::matchRight( F f ) const noexcept( noexcept(f() ))
{
    return map( f );
}

template<typename L>
template<typename F>
void Either<L,void>::matchLeft( F f ) const noexcept( noexcept( f( std::declval<L>() )))
{
    if( isLeft() )
    {
        f( getLeft() );
    }
}
