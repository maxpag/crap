# /*
g++ -Wall -Wextra -g -std=gnu++2a \
    -DBUILD_RELEASE \
    -DChefBase_FULL_ASSERT \
    -I../PlatformAgnostic  \
    -I../PlatformSpecific/generic_linux \
    -I../PlatformSpecific/x86_generic \
    ../PlatformAgnostic/ChefBase/Error.cc \
    $0
./a.out "$@"
return
# */
/**
 * @addtogroup ChefTest
 * @{
 * @file Test/ChefBase/ErrorTest.cc
 * @author Massimiliano Pagani
 * @version 1.0
 * @date 2020-03-19
 *
 */

/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <ChefTest/Framework.hh>
#include <ChefBase/Error.hh>

ChefTest_TEST_SUITE( "ChefBase/Error" )

    using namespace ChefBase;

ChefTest_TEST_CASE( "constructions" )
{
    ChefTest_ASSERT( std::is_copy_constructible<Error>::value );
    ChefTest_ASSERT( std::is_copy_assignable<Error>::value );
    ChefTest_ASSERT( std::is_move_constructible<Error>::value );
    ChefTest_ASSERT( std::is_move_assignable<Error>::value );
}

ChefTest_TEST_CASE( "operator==" )
{
    constexpr unsigned ErrorA = 0xCA;
    constexpr unsigned ModuleA = 0xFE;
    constexpr unsigned DataA = 0xBABE;

    constexpr unsigned ErrorB = 0xDE;
    constexpr unsigned ModuleB = 0xAD;
    constexpr unsigned DataB = 0xBEEF;

    auto a0 = Error{ ErrorA, ModuleA, DataA };
    auto a1 = Error{ ErrorA, ModuleA, DataA };

    ChefTest_ASSERT( a0 == a0 );
    ChefTest_ASSERT( a0 == a1 );

    auto b0 = Error{ ErrorA, ModuleA, DataB };
    auto b1 = Error{ ErrorA, ModuleB, DataA };
    auto b2 = Error{ ErrorB, ModuleA, DataA };

    ChefTest_ASSERT( !(a0 == b0) );
    ChefTest_ASSERT( !(a0 == b1) );
    ChefTest_ASSERT( !(a0 == b2) );
}

ChefTest_TEST_CASE( "toEither" )
{
    constexpr unsigned Ok = 0;
    constexpr unsigned Fail = 1;
    constexpr unsigned Answer = 42;

    auto good = Error{ Ok, 0, 0 };
    auto expectedRight = good.toEither<unsigned>( Answer );
    ChefTest_ASSERT( expectedRight.isRight() );
    ChefTest_ASSERT( expectedRight.getRight() == Answer );

    auto bad = Error{ Fail, 0, 0 };
    auto expectedLeft = bad.toEither<unsigned>( Answer );
    ChefTest_ASSERT( expectedLeft.isLeft() );
    auto leftError = expectedLeft.getLeft();
    ChefTest_ASSERT( leftError == bad );
}

ChefTest_TEST_CASE( "andThen" )
{
    constexpr unsigned Ok = 0;
    constexpr unsigned Fail = 1;

    auto good = Error{ Ok, 0, 0 };
    auto bad = Error{ Fail, 0, 0 };
    ChefTest_ASSERT( good.andThen( [=](){ return good; }).getCode() == Ok );
    ChefTest_ASSERT( good.andThen( [=](){ return bad; }).getCode() == Fail );

    ChefTest_ASSERT( bad.andThen( [=](){ return good; }).getCode() == Fail );
    ChefTest_ASSERT( bad.andThen( [=](){ return bad; }).getCode() == Fail );
}

ChefTest_TEST_CASE( "orElse" )
{
    constexpr unsigned Ok = 0;
    constexpr unsigned Fail = 1;

    auto good = Error{ Ok, 0, 0 };
    auto bad = Error{ Fail, 0, 0 };
    ChefTest_ASSERT( good.orElse( [=](auto&&){ return good; }).getCode() == Ok );
    ChefTest_ASSERT( good.orElse( [=](auto&&){ return bad; }).getCode() == Ok );

    auto r1 = bad.orElse( [=](auto&& left){
                     ChefTest_ASSERT(left == bad);
                     return good;
                 }).getCode();
    ChefTest_ASSERT_EQUAL( r1, Ok );

    auto r2 = bad.orElse( [=](auto&& left){
                     ChefTest_ASSERT(left == bad);
                     return bad;
                 }).getCode();
    ChefTest_ASSERT_EQUAL( r2, Fail );
}
