# /*
g++ -Wall -I. -g -std=gnu++17 \
    -DCHEFLIB_X86_TEST \
    -DCHEFLIB_DEBUG_FULL_ASSERT \
    -DBUILD_RELEASE \
    -I ../PlatformAgnostic \
    ChefFun/OptionTest.cc
./a.out "$@"
return
# */
/**
 * @addtogroup ChefTest
 * @{
 * @file ChefTest/CcLibs_Option.cc
 * @author Massimiliano Pagani
 *
 */

/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <ChefFun/Option.hh>
#include <string>
#include <ChefTest/Framework.hh>

using namespace ChefFun;

ChefTest_TEST_SUITE( "ChefFun/Option")

ChefTest_TEST_CASE( "copy constructors" )
{
    auto a = Option<int>::Some(3);
    Option<int> b{a};

    ChefTest_ASSERT( b.isDefined() );
    ChefTest_ASSERT_EQUAL( b.get(), 3 );

    auto c = Option<int>::None();
    Option<int> d{c};
    ChefTest_ASSERT( c.isEmpty() );
}

ChefTest_TEST_CASE( "base test" )
{
    typedef Option<int> OptionInt;

    OptionInt a = None<int>();
    ChefTest_ASSERT( a.isEmpty() );

    bool caught=false;
    try
    {
        int x = a.move();
        (void)x;
        assert( false );
    }
    catch( ... )
    {
        caught = true;
    }
    assert( caught );

    a = Some(3);
    ChefTest_ASSERT( a.isDefined() );
    OptionInt b{ OptionInt::Some(3)};
    ChefTest_ASSERT( b.isDefined() );
    ChefTest_ASSERT_EQUAL( None<int>().getOrElse(3), 3 );
    ChefTest_ASSERT_EQUAL( OptionInt::Some(42).getOrElse(3), 42 );

    int c = 1+b.get();
    ChefTest_ASSERT_EQUAL( c, 1+3 );
}

static int f( std::string const&  x )
{
    return x.length();
}

ChefTest_TEST_CASE( "vararg constructor" )
{
    class Test
    {
        public:
            Test( int x, int y ) : m_x{x},m_y{y} {}
            int m_x;
            int m_y;
    };

    auto o = Option<Test>::Some( 4, 2 );
    ChefTest_ASSERT( o.isDefined() );
    ChefTest_ASSERT( o.get().m_x == 4 );
    ChefTest_ASSERT( o.get().m_y == 2 );
}

ChefTest_TEST_CASE( "map test" )
{
    Option<std::string> test = Option<std::string>::Some( std::string{"abc"});
    Option<int> result = test.map( f );
    ChefTest_ASSERT( result.isDefined() );
    ChefTest_ASSERT_EQUAL( result.get(), 3 );
}

ChefTest_TEST_CASE( "assignment" )
{
    Option<int> a{ Option<int>::Some(3) };
    Option<int> b{ Option<int>::Some(2) };

    a = b;
    ChefTest_ASSERT( a.isDefined() );
    ChefTest_ASSERT( b.isDefined() );
    ChefTest_ASSERT( a.get() == 2 );
    ChefTest_ASSERT( b.get() == 2 );
    ChefTest_ASSERT( a == b );

    Option<int> c{ Option<int>::None() };
    a = c;
    ChefTest_ASSERT( a.isEmpty() );
    ChefTest_ASSERT( a == c );

    a = b;
    ChefTest_ASSERT( a.isDefined() );
    ChefTest_ASSERT( b.isDefined() );
    ChefTest_ASSERT( a.get() == 2 );
    ChefTest_ASSERT( b.get() == 2 );
    ChefTest_ASSERT( a == b );

    a = c;
    // both a and c are None
    c = a;
    ChefTest_ASSERT( a.isEmpty() );
    ChefTest_ASSERT( c.isEmpty() );

}

ChefTest_TEST_CASE( "foreach" )
{
    auto a = Option<int>::Some(3);
    auto b = Option<int>::None();

    bool isCalled = false;
    a.foreach( [&isCalled]( int x ){ isCalled = true; });
    ChefTest_ASSERT( isCalled );

    isCalled = false;
    b.foreach( [&isCalled]( int x ){ isCalled = true; });
    ChefTest_ASSERT( !isCalled );
}

ChefTest_TEST_CASE( "begin/end" )
{
    auto a = Option<int>::Some(3);
    auto b = Option<int>::None();

    bool isExecuted = false;
    for( auto x : a )
    {
        isExecuted = true;
        ChefTest_ASSERT( x == 3 );
    }
    ChefTest_ASSERT( isExecuted );

    isExecuted = false;
    for( auto x : b )
    {
        (void)x;
        isExecuted = true;
    }
    ChefTest_ASSERT( !isExecuted );
}

ChefTest_TEST_CASE( "flatten" )
{
    auto a = Option<Option<int>>::Some(Option<int>::Some(3));
    auto b = Option<Option<int>>::Some(Option<int>::None());
    auto c = Option<Option<int>>::None();

    ChefTest_ASSERT( a.flatten() == Option<int>::Some(3) );
    ChefTest_ASSERT( b.flatten() == Option<int>::None() );
    ChefTest_ASSERT( c.flatten() == Option<int>::None() );
}

ChefTest_TEST_CASE( "pattern matching - functional" )
{
    auto a = Option<int>::Some(3);
    auto b = Option<int>::None();

    int result0 = a
        .matchSome( []( auto&& n ){ return n*2; } )
        .matchNone( [](){ return 0; } );

    ChefTest_ASSERT_EQUAL( result0, 6 );

    int result1 = b
        .matchSome( []( auto&& n ){ return n*2; } )
        .matchNone( [](){ return 0; } );

    ChefTest_ASSERT_EQUAL( result1, 0 );
}

ChefTest_TEST_CASE( "pattern matching - imperative" )
{
    auto a = Option<int>::Some(3);
    auto b = Option<int>::None();

    int result{};

    a
        .matchSome( [&result]( auto&& n ){ result = n*2; } )
        .matchNone( [&result](){ result = 0; } );

    ChefTest_ASSERT_EQUAL( result, 6 );

    b
        .matchSome( [&result]( auto&& n ){ result = n*2; } )
        .matchNone( [&result](){ result = 0; } );

    ChefTest_ASSERT_EQUAL( result, 0 );
}


///@}
