#!/usr/bin/env bash
#
# (C) 2022 Massimiliano Pagani
# 
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
#     http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

scriptDir="$(dirname "$0")"
crapLibDir="$scriptDir/.."
testDir="$crapLibDir/library/Test"

cd "$testDir" || (echo "Can't find test directory" ; exit 1 )

runAllTests=yes
packages=()
while [ -n "$1" ]
do
  runAllTests=no
  file="${1}Test.cc"
  if [ -e "$file" ]
  then
    packages+=("$file")
  else
    echo "Ignoring unknown test '$1'"
  fi
  shift
done

if [ "$runAllTests" = "yes" ]
then
  # all tests
  mapfile -t packages < <(find . -name "*.cc")
fi
for testName in "${packages[@]}"
do
  if [ -e "a.out" ]
  then
    rm "a.out"
  fi
  # shellcheck source="$testName"
  . "$testName"
done
